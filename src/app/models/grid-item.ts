export interface GridItem {
  editable: boolean;
  value: string;
}
